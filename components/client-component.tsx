'use client'

import { useEffect, useState } from 'react'

// u can either get the data via Route Handler or fetch here via render-then-fetch method. or use 3rd party libraries like SWR (preferable)

export default function ClientComponent() {
  const [jokeData, setJokeData] = useState<any>({})

  useEffect(() => {
    // ignore is used for cleanup, learn more here: https://react.dev/learn/you-might-not-need-an-effect#fetching-data
    let ignore = false

    fetch('https://v2.jokeapi.dev/joke/Programming?type=single', {
      cache: 'no-store',
    })
      .then((response) => response.json())
      .then((data) => {
        if (!ignore) {
          setJokeData(data)
        }
      })
      .catch((error) => {
        throw new Error(error)
      })

    return () => {
      ignore = true
    }
  }, [])

  return (
    <div>
      <h2>Client Component</h2>

      <p>{Object.keys(jokeData).length === 0 ? 'loading..' : jokeData?.joke}</p>
    </div>
  )
}
