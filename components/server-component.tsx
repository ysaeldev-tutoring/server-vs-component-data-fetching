export default async function ServerComponent() {
  const jokeData = await fetch(
    'https://v2.jokeapi.dev/joke/Programming?type=single',
    { cache: 'no-store' }
  )
    .then((response) => response.json())
    .catch((error) => {
      throw new Error(error)
    })

  return (
    <div>
      <h2>Server Component</h2>

      <p>{jokeData?.joke}</p>
    </div>
  )
}
