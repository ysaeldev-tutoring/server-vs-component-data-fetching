import ClientComponent from '@/components/client-component'
import ServerComponent from '@/components/server-component'

export default function Home() {
  return (
    <main className="flex flex-col gap-4 p-8">
      <h1>Async/Await vs Fetch </h1>

      <p>
        Here we have 2 components, a server component and a client component.
        Lets see the diff between fetching data
      </p>

      <ServerComponent />

      <ClientComponent />
    </main>
  )
}
