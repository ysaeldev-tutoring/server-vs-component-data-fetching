This project is to showcase the difference of data fetching between server component and client components

## Getting Started

First, install dependencies, then run the development server:

```bash
bun i
```

then

```bash
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## The Data Source

The data is from the [JokeAPI](https://v2.jokeapi.dev/)
